use std::fs; 
use std::env;
pub fn read_input (name: &str) -> String{
    let file_path = String::from("puzzle_inputs/") + &String::from(name);
    println!("Current directory {}", env::current_dir().unwrap().display());
    println!("Reading in file {}", file_path);
    let contents = fs::read_to_string(file_path).expect("Ya dun messed up A-aoron and we couldn't read the file");
    contents
}