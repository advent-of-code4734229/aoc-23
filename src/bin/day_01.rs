use aoc::read_input;
fn main() {
    // running part one
    let input = read_input("day_01.txt");
    let output = parse_1(&input);
    println!("{}",output)
}

pub fn parse_1 (input: &str) -> i32 {
    let calories: Vec<&str>= input.split("\n").collect();
    // println!("{:?}", calories);
    let mut sum = Vec::new();
    sum.push(0);
    let mut increment:usize = 0;
    for cal in calories {
        let num = cal.trim().parse::<i32>();
        match num {
            Ok(val) => sum[increment] += val,
            Err(_) => {
                sum.push(0);
                increment += 1;
            }
        }
    }
    sum.iter().max().unwrap().to_owned()
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    pub fn test_part_1() {
        let input = r#"1000
        2000
        3000

        4000

        5000
        6000

        7000
        8000
        9000

        10000"#;
        let max = parse_1(&input);
        println!("{}", max);
        assert!(max == 24000)
    }
}