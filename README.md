# aoc-22

My repository for working through advent of code problems in Rust for 2023


# Other Solutions
A collection of solutions I have been looking to in order to learn more "idomatic" ways to solve problems
- [Treuille](https://github.com/treuille/advent-of-code-2022-in-rust/tree/main/src/bin) 